module.exports = {
  // Auto discover
  autodiscover: true,

  // Filter groups for auto discovery
  // Anything under gitlab-com/gl-infra, except:
  // 1. gitlab-dedicated
  // 2. common-ci-tasks
  // 3. cmbr
  // 4. terra-transformer
  // 5. pmv
  // 6. bigquery-loader
  // 7. asdf-promtool
  // 8. asdf-gl-infra
  // 9. slackline
  autodiscoverFilter: [
    "gitlab-com/gl-infra/!(gitlab-dedicated|common-ci-tasks|cmbr|terra-transformer|pmv|bigquery-loader|asdf-promtool|asdf-gl-infra|tamland|slackline|us-public-sector){*,/**}",
  ],

  // Do not onboard
  onboarding: false,

  // Require config in repo
  requireConfig: "required",

  // Host rules as ENV vars - https://docs.renovatebot.com/self-hosted-configuration/#detecthostrulesfromenv
  detectHostRulesFromEnv: true,

  // Allow all post-upgrade commands
  allowPostUpgradeCommandTemplating: true,
  allowedPostUpgradeCommands: [".*"],
};
