require 'gitlab'
require 'time'
require 'json'
require 'yaml'

g = Gitlab.client(
  endpoint: 'https://gitlab.com/api/v4',
  private_token: ENV["RENOVATE_TOKEN"],
)

projects = YAML.load_file(ENV["PROJECTS_YAML"])

message = {
  "blocks" => [
    {
      "type" => "header",
      "text" => {
        "type" => "plain_text",
        "text" => ":frog-alert: There are :renovate: Renovate MRs older than 1 month: :alert: :tanuki-sparkle-2:"
      }
    }
  ]
}

send_alert = false

projects.each do |project_id|
  mrs = g.merge_requests(project_id, {author_username: 'glrenovatebot', state: 'opened'})
  t = Time.now

  block_text = []

  mrs.each do |mr|
    created = Time.parse(mr.created_at)
    age = t - created
    block_text.append("<#{mr.web_url}|##{mr.iid} - #{mr.title}>") if age > 30 * 24 * 3600 # 30 days
  end

  unless block_text.empty? then
    message["blocks"].append({
      "type" => "header",
      "text" => {
        "type" => "plain_text",
        "text" => "#{g.project(project_id).name}:"
      }
    })
    message["blocks"].append({
      "type" => "section",
      "text" => {
        "type" => "mrkdwn",
        "text" => block_text.join("\n")
      }
    })
    send_alert = true
  end
end

puts message.to_json

HTTParty.post(ENV["SLACK_WEBHOOK_URL"], body: message.to_json, headers: { 'Content-Type' => 'application/json' }) if send_alert
